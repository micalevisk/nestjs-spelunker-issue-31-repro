import { Module, forwardRef } from '@nestjs/common';
import { A } from './a.module'
import { BService } from './b.service';

@Module({
  imports: [forwardRef(() => A)],
  providers: [BService],
  exports: [BService],
})
export class B {}
