import { Module } from '@nestjs/common';
import { A } from './a.module'
import { B } from './b.module'

@Module({
  imports: [A, B]
})
export class AppModule {}
