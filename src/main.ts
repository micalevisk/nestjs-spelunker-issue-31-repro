import { NestFactory } from '@nestjs/core';
import type { NestExpressApplication } from '@nestjs/platform-express';
import { SpelunkerModule } from 'nestjs-spelunker'
import { AppModule } from './app.module';

/*
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
*/

async function bootstrap(){
  console.log(await SpelunkerModule.debug(AppModule))
}
bootstrap()
