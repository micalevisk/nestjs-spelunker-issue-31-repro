import { Module, forwardRef } from '@nestjs/common';
import { B } from './b.module'
import { AService } from './a.service';

@Module({
  imports: [forwardRef(() => B)],
  providers: [AService],
  exports: [AService]
})
export class A {}
